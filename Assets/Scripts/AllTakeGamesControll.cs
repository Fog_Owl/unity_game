﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AllTakeGamesControll : MonoBehaviour
{
    public static AllTakeGamesControll instance = null;
    public 
    int levelComplete;
    int sceneIndex;
    void Start()
    {
        sceneIndex = SceneManager.GetActiveScene().buildIndex; 
        levelComplete = PlayerPrefs.GetInt("LevelComplete");

    }
    public void Endgame()
    {
       
        Debug.Log(levelComplete);
        Debug.Log(sceneIndex);
        if(sceneIndex == 3)
        {
            Invoke("LoadMainMenu",1f);
        }
        else if (levelComplete <= sceneIndex)
        {
            
                PlayerPrefs.SetInt("LevelComplete", sceneIndex);
                Invoke("NextLevel", 1f);
            
            
        }
        else if(sceneIndex < levelComplete)
        {
            Invoke("NextLevel", 1f);
        }
    }
    void NextLevel()
    {
        SceneManager.LoadScene(sceneIndex + 1);
    }
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("Меню");
    }
    public void PlayOneMore()
    {
        SceneManager.LoadScene(sceneIndex);
    }
    
}
