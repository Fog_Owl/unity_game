﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using JetBrains.Annotations;

public class Sound_controller : MonoBehaviour
{
    AudioSource m_MyAudioSource;
    public Slider sound;
    void Start()
    {
        m_MyAudioSource = GetComponent<AudioSource>();
        m_MyAudioSource.Stop();
        m_MyAudioSource.Play();

    }
    
    private void Awake()
    {
        
        DontDestroyOnLoad(gameObject);
    }


    public void TakeVolume()
    {
        
        AudioListener.volume = sound.value;
    }
}
