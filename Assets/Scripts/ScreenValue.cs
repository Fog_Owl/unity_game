﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class ScreenValue : MonoBehaviour
{   
    public GameObject tryHard;
    // Start is called before the first frame update
    void Start()
    {
        float ratio = (float)Screen.height / Screen.width;
        var rty = tryHard.GetComponent<Renderer>();
        int width = (int)rty.bounds.size.x;
        float ver = ratio * width;
        float ortSize = ver / 200f;
        Camera.main.orthographicSize = ortSize;


    }


}
