﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float jumpForse;
    private float moveInput;
    private Rigidbody2D rb;
    public GameObject level;
    public GameObject dielevel;



    public Joystick joystick;
    public Text window_interface;
    public GameObject[] count_circle;

    private void Start() 
    {
        rb = GetComponent<Rigidbody2D>();
       
    }
    private void FixedUpdate()
    {
        moveInput = joystick.Horizontal;
        
        rb.AddForce(new Vector2(moveInput * speed, rb.velocity.y));
        

    }

    public void OnJumpButtonDown()
    {
        rb.velocity = Vector2.up * jumpForse;
    }
    private void Update()
    {
        var count_circle = GameObject.FindGameObjectsWithTag("circle_power");
        window_interface.text = "Осталось колец силы:   " + count_circle.Length;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "circle_power")
        {
            Destroy(collision.gameObject);
            //collision.gameObject.SetActive(false);
        }

         if (collision.gameObject.tag == "ships")
        {
            level.SetActive(false);
            dielevel.SetActive(true);
        }
        if (collision.gameObject.tag == "trigger_Bsmall")
        {
            transform.localScale -= new Vector3(0.2f, 0.2f, 0.0f);
            collision.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
        if (collision.gameObject.tag == "trigger_Bbig")
        {
            transform.localScale += new Vector3(0.6f, 0.6f, 0.0f);
            collision.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
        if (collision.gameObject.tag == "trigger_Bnormal")
        {
            transform.localScale -= new Vector3(0.4f, 0.4f, 0.0f);
            collision.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }

    }
    


}
