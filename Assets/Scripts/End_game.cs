﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class End_game : MonoBehaviour
{
    public GameObject window1;
    public GameObject endlook;
    public GameObject firstlevel;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("Конец");
        }
        if (GameObject.FindGameObjectWithTag("circle_power") == null)
        {
            endlook.SetActive(true);
            firstlevel.SetActive(false);
        }
        if (GameObject.FindGameObjectWithTag("circle_power") != null)
        {
            window1.SetActive(true);
            Time.timeScale = 0;
        }
    }
    public void PlayGame1()
    {
        Time.timeScale = 1;
        window1.SetActive(false);
        
    }
    
    private void Update()
    {
        
    }
}
