﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Takelvl : MonoBehaviour
{
    // Start is called before the first frame update



    public Button FirstLevel;
    public Button SecondLevel;
    public Button ThirdLevel;

    int levelComplete;
    void Start()
    {
        levelComplete = PlayerPrefs.GetInt("LevelComplete");
        SecondLevel.interactable = false;
        ThirdLevel.interactable = false;

        switch (levelComplete)
        {
            case 1:
                SecondLevel.interactable = true;
                break;
            case 2:
                SecondLevel.interactable = true;
                ThirdLevel.interactable = true;
                break;
        }
    }
    public void LoadLevel(int level)
    {
        SceneManager.LoadScene(level);
    }
    public void Reset()
    {
        SecondLevel.interactable = false;
        ThirdLevel.interactable = false;
        PlayerPrefs.DeleteAll();
    }
}
