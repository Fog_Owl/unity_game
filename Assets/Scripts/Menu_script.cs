﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu_script : MonoBehaviour
{
   
    public GameObject ChooseLevel;
    int levelComplete;
    public GameObject levelsettings;
    public GameObject menu;
    

    
    // Start is called before the first frame update
    void Start()
    {
       
    }
    
    


    public void PressSettings()
    {

    }
    public void PressLevel()
    {
        levelComplete = PlayerPrefs.GetInt("LevelComplete");
        ChooseLevel.SetActive(true);
        this.gameObject.SetActive(false);
        Debug.Log(levelComplete);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void LoadMenuSettings()
    {
        levelsettings.SetActive(true);
        this.gameObject.SetActive(false);
    }
    public void LoadMenu()
    {
        menu.SetActive(true);
        levelsettings.SetActive(false);
        ChooseLevel.SetActive(false);
    }
}
